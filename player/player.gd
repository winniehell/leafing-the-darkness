extends KinematicBody2D

export var speed = 0

func _input(event):
	var velocity = Vector2(
		event.get_action_strength("ui_right") - event.get_action_strength("ui_left"),
		event.get_action_strength("ui_down") - event.get_action_strength("ui_up")
	) * speed

	if velocity.x != 0:
		$Sprite.flip_h = velocity.x < 0

	move_and_slide(velocity)

func broadcast_position():
	get_tree().call_group(GroupNames.follows_player, "update_player_position", global_position)
