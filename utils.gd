extends Node

func is_player(body: Node) -> bool:
	return body.name == 'Player'
