extends Node2D

func _ready():
	$Light2D.hide()

func body_entered(body):
	if !Utils.is_player(body):
		return
	
	if get_tree().get_nodes_in_group(GroupNames.follows_player).empty():
		return
	
	get_tree().call_group(GroupNames.follows_player, "stop_following", $Light2D.global_position)
	$Sprite.material = null
	$Light2D.show()
