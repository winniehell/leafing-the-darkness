extends Node2D

var next_global_position

func _ready():
	next_global_position = global_position
	$PositionTween.start()

func body_entered(body):
	if !Utils.is_player(body):
		return

	$Area2D/CollisionShape2D.disabled = true
	add_to_group(GroupNames.follows_player)

func update_player_position(global_player_position):
	next_global_position = global_player_position

func position_tween_completed():
	$PositionTween.interpolate_property(
		self, 
		"global_position",
		global_position,
		next_global_position,
		0.5
	)
	$PositionTween.start()

func stop_following(new_position):
	remove_from_group(GroupNames.follows_player)
	$PositionTween.stop_all()
	next_global_position = new_position
	position_tween_completed()
